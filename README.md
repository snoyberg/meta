# Haskell Foundation Ways of Working

This "meta" repo contains notes about how the Haskell Foundation
conducts its business. If you want to change the way the Haskell
Foundation does its business, please make an MR or file an Issue
against this repo.

