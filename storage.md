# Storage

This document describes the practices of how the Haskell Foundation stores
its digital assets.

1. The Haskell Foundation maintains a [Google Drive
   folder](https://drive.google.com/drive/folders/1gxc2miCWM0gwMoA7ywUH3qY--q277Ifc?usp=sharing)
   and a [GitLab group](https://gitlab.haskell.org/hf). Google Drive is good
   for co-editing documents; GitLab is good for long-term archiving and public
   release.

2. All Board members and the Executive Team have read/write access to both the
   Drive folder and the GitLab group. The Chair, the Treasurer, and the Executive
   Director have the ability to add/remove additional individuals.

3. Sub-parts of both the Drive folder and the GitLab group may be made
   public by a decision of the Board.

4. It may be convenient to grant individuals who are not on the Board and not
   employed by the Foundation full access to these resources; any such
   decision will be documented with a rationale.

## Current status

The GitLab group currently hosts three repositories:

* `meta`: Details of how the Haskell Foundation conducts its business. World-readable.

* `minutes`: Ratified minutes of official meetings. World-readable.

* `proposals`: Accepted proposals for what the Haskell Foundation should seek to do.
  Merge Requests in this repository are pending proposals. World-readable.
