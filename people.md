# Who we are

This document describes the current and former people involved with
the Haskell Foundation.

## Current staff

<!-- edit these easily by pasting it at https://www.tablesgenerator.com/markdown_tables -->

| Name            | Appointment | Role                     | Email | Twitter | GitHub | gitlab.haskell.org |
|-----------------|-------------|--------------------------|-------|---------|--------|--------------------|
| Andrew Boardman | Feb 2021    | Executive Director       |       |         |        |                    |
| Emily Pillmore  | Feb 2021    | Chief Technology Officer |       |         |        |                    |

## Current Board of Directors

| Name                 | Appointment | Affiliation      | Role      | Email                | Twitter        | GitHub                 | gitlab.haskell.org |
|----------------------|-------------|------------------|-----------|----------------------|----------------|------------------------|--------------------|
| Alexander Bernauer   | Feb 2021    | PwC Switzerland  |           |                      |                |                        |                    |
| Théophile Choutri    | Feb 2021    |                  |           | theophile@choutri.eu | @TechnoEmpress | @Kleidukos / @tchoutri | @Kleidukos         |
| Scott Conley         | Feb 2021    |                  |           |                      |                |                        |                    |
| Wendy Devolder       | Feb 2021    |                  |           |                      |                |                        |                    |
| Richard Eisenberg    | Feb 2021    | Tweag I/O        | Chair     | rae@richarde.dev     | @RaeHaskell    | @goldfirere            | @rae               |
| Tom Ellis            | Feb 2021    |                  |           |                      |                |                        |                    |
| Edward Kmett         | Feb 2021    |                  |           |                      |                |                        |                    |
| Andrew Lelechenko    | Feb 2021    |                  |           |                      |                |                        |                    |
| Jose Pedro Magalhaes | Feb 2021    |                  |           |                      |                |                        |                    |
| Simon Peyton Jones   | Feb 2021    |                  |           |                      |                |                        |                    |
| Michael Snoyman      | Feb 2021    | FP Complete      |           | michael@snoyman.com  | @snoyberg      | @snoyberg              | @snoyberg          |
| Ryan Trinkle         | Feb 2021    | Obsidian Systems | Treasurer |                      |                |                        |                    |
| Niki Vazou           | Feb 2021    |                  |           |                      |                |                        |                    |

## Past members of the Haskell Foundation

We are grateful to these members of the Interim Board, who selected
the initial Board of Directors and our initial staff.

| Name                 | Affiliation                | Role  |
|----------------------|----------------------------|-------|
| Lennart Augustsson   | Epic Games                 |       |
| Chris Dornan         | IRIS Connect               |       |
| Gabriele Keller      | Universiteit Utrecht       |       |
| Edward Kmett         | MIRI                       |       |
| Simon Marlow         | Facebook                   |       |
| Simon Peyton Jones   | Microsoft Research         | Chair |
| Jasper Van Der Jeugt | Fugue                      |       |
| Stephanie Weirich    | University of Pennsylvania |       |

Beyond just these members of our interim Board, we thank the many
volunteers who helped build the idea that became the Haskell Foundation.
In particular, we thank Tim Sears, Emily Pillmore, and Ryan Trinkle, who did much of
the hard, often invisible work of organizing us and arranging our founding
sponsors.
