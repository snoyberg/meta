# Meetings

This document describes the processes for all official Haskell Foundation
meetings, including both meetings of the Board and meetings of Task Forces
and Committees.

## General Terms

1. Meetings are generally called by the Chair. It is expected that the Chair
   be responsive to a request from another Member to call a meeting, but if
   the Chair is unresponsive, any Member can call a meeting.

2. Meetings are scheduled in a manner as equitable as possible to our varied
   membership. Specifically, meetings will not be scheduled in such a way that
   any one Member is regularly prevented from participating in all meetings.
   
## Agendas

1. All meetings have an agenda posted beforehand by the Member who called
   the meeting (generally the Chair), ideally at least 48 hours before the
   meeting time.

2. Agendas are posted in the appropriate folder in Google Drive.

3. Agendas may be edited in advance of the meeting to add items or clarifying questions.

4. Board Members will not remove others' agenda items or otherwise subvert agendas.

5. After the meeting, the agenda is be made public by being added to the
   [minutes repository](https://gitlab.haskell.org/hf/minutes).

## Minutes

See also our [Transparency](transparency.md) notes.

1. Minutes are taken for all official meetings.

2. For Task Force meetings, the Leader of the Task Force acts as its Secretary.

3. The Secretary creates the minutes document in Google Drive before the meeting.

4. All meeting participants are expected to collaboratively contribute to the
   meeting minutes. The Secretary is ultimately responsible for the accurate
   capturing of the meeting minutes.

5. Attendees of the minutes edit the minutes within 24 hours of the meeting
   to correct any misquotes, etc., that were recorded.

5. The Secretary is responsible to review the minutes after the meeting to
   ensure they are appropriate for publishing.

8. Minutes are automatically ratified 24 hours after the end of the meeting or
   as soon as all pending objections have been resolved, whichever comes
   first.

9. The Secretary is responsible for publishing the minutes in a timely manner
   after ratification.
