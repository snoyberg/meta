# Transparency

This document sets out the transparency principles and practices of the
Haskell Foundation (HF).

## Principles

1. The Haskell Foundation is a servant of the greater Haskell community; accordingly,
   it must report its work to the greater Haskell community.

2. The Haskell Foundation's success depends on working well with community members;
   accordingly, the public must be able to learn what the HF is
   doing, so as not to make redundant effort.

3. The Haskell Foundation depends on the trust of the community; accordingly, the
   public must be able to audit what the Haskell Foundation has done, to ensure
   this trust is warranted.

4. The Haskell Foundation is fed with ideas from the community; accordingly, there
   must be a way for the community to engage with the HF.

5. The Haskell Foundation is tasked with supporting the Haskell ecosystem
   and infrasturcture; accordingly, the goal of transparency must not supersede
   its other goals.

## Practices

1. All Board meetings and official Task Force or Committee meetings have
   minutes taken. These minutes are posted between 24 hours and 1 week
   after the meeting, with a preference for a shorter lag. Minutes (and,
   agendas, when available) are posted in the
   [hf/minutes](https://gitlab.haskell.org/hf/minutes) repository.

   There is the possibility that some details are redacted in the minutes
   before posting. These details might concern, for example, personnel decisions
   or not-yet-closed fund-raising negotations.

2. All Board meetings and official Task Force or Committee meetings are
   posted on the official Haskell Foundation calendar. The public
   may access this [calendar](https://calendar.google.com/calendar/u/1?cid=Mmc5OXI0azIxam02aXNyZmQyNmhpdTQ0cWNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ).

3. At least once every two months, there is a public meeting of the Haskell
   Foundation. These are posted on the HF calendar and advertised online. The
   goal of these meetings is to make HF Board members and employees available
   to the public to provide an update on the HF, to answer questions, and
   to source ideas from the community.

   In a nod toward efficiency, other meetings of the Board are not open to the public.

4. Communication within the HF is via GNU Mailman [mailing lists](https://mail.haskell.org/mailman/listinfo). (TODO: Move these lists from `haskell.org` to `haskell.foundation`.)
   The main mailing list of the Board is `board@lists.haskell.foundation`; other
   sub-groups have their own lists, as viewable at the link above. Long-running
   Task Forces and Committees will get their own mailing lists. All archives of
   these lists are public, and the public may write emails to the various groups
   by writing to the mailing list addresses directly; such emails will be moderated,
   but relevant ones (as determined by a list moderator) will be delivered.

   Certain sensitive topics (e.g. personnel) are communicated by direct email
   to the Board members; these emails are not archived.

5. The Haskell Foundation maintains a [proposals](https://gitlab.haskell.org/hf/proposals)
   repository, where anyone can submit a proposal for an idea that the HF should
   pursue. This reposotory is the one official way to request that the HF take
   a specific action.

6. The [Haskell Discourse instance](https://discourse.haskell.org/) maintains
   a [Haskell Foundation category](https://discourse.haskell.org/c/haskell-foundation/11)
   for use in discussing HF matters, for example, to get early feedback on a proposal
   idea. The HF will be sure to monitor this channel of communication.

7. A yearly "State of the Haskell Foundation" report is compiled, posted on
   the [Haskell Foundation website](https://haskell.foundation/). TODO: When
   in the year will this be done?

